# Authors: William Brown <william@firstyear.id.au>
#
# Copyright (C) 2007  Red Hat
# see file 'COPYING' for use and warranty information
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ipalib.plugins.baseldap import *
from ipalib import _, api, errors

__doc__ = _("""
""")


#Params can be made optional with str('foo?',
#Some of these values can be INT's not just STR


class dhcpserver(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Server')
    object_name_plural = _('DHCP Servers')
    object_class = ['dhcpServer', 'top']
    default_attributes = ['cn']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Servers')
    label_singular = _('DHCP Server')
    takes_params = (
        Str('cn',
            cli_name='server_name',
            label=_('DHCP Server name'),
            primary_key=True,
        ),
        Str('dhcpservicedn',
            cli_name='server_service_dn',
            label=_('DHCP Server Service DN'),
        ),
        Str('dhcplocatordn',
            cli_name='server_locator_dn',
            label=_('DHCP Server Locator DN'),
        ),
        Str('dhcpversion',
            cli_name='server_version',
            label=_('DHCP Server Version'),
        ),
        Str('dhcpimplementation',
            cli_name='server_implementation',
            label=_('DHCP Server implementation'),
        ),
        Str('dhcphashbucketassignment',
            cli_name='server_hash_bucket_assignment',
            label=_('DHCP Server Hash bucket assignment'),
        ),
        Str('dhcpdelayedserviceparameter',
            cli_name='server_delayed_service_parameter',
            label=_('DHCP Server Delayed Service Parameter'),
        ),
        Str('dhcpmaxclientleadtime',
            cli_name='server_max_client_lead_time',
            label=_('DHCP Server Max Client Lead Time'),
        ),
        Str('dhcpfailoverendpointstate',
            cli_name='server_fail_over_endpoint_state',
            label=_('DHCP Server Fail Over Endpoint State'),
        ),
        Str('dhcpstatements',
            cli_name='server_statements',
            label=_('DHCP Server Statements'),
        ),
        Str('dhcpcomments',
            cli_name='server_comments',
            label=_('DHCP Server Comments'),
        ),
        Str('dhcpoption',
            cli_name='server_option',
            label=_('DHCP Server Option'),
        ),

    )

api.register(dhcpserver)
    
class dhcpservice(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Service')
    object_name_plural = _('DHCP Servicess')
    object_class = ['dhcpService', 'top']
    default_attributes = ['cn']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Services')
    label_singular = _('DHCP Service')
    takes_params = (
        Str('cn',
            cli_name='service_name',
            label=_('DHCP Service Name'),
            primary_key=True,
        ),
        Str('dhcpprimarydn',
            cli_name='service_primary_dn',
            label=_('DHCP Service Primary DN'),
        ),
        Str('dhcpsecondarydn',
            cli_name='service_secondary_dn',
            label=_('DHCP Service Secondary DN'),
        ),
        Str('dhcpserverdn',
            cli_name='service_server_dn',
            label=_('DHCP Service Server DN'),
        ),
        Str('dhcpsharednetworkdn',
            cli_name='service_shared_network_dn',
            label=_('DHCP Service Shared Network DN'),
        ),
        Str('dhcpsubnetdn',
            cli_name='service_subnet_dn',
            label=_('DHCP Service Subnet DN'),
        ),
        Str('dhcpgroupdn',
            cli_name='service_group_dn',
            label=_('DHCP Service Group DN'),
        ),
        Str('dhcphostdn',
            cli_name='service_host_dn',
            label=_('DHCP Service Host DN'),
        ),
        Str('dhcpclassesdn',
            cli_name='service_classes_dn',
            label=_('DHCP Service Classes DN'),
        ),
        Str('dhcpoptionsdn',
            cli_name='service_options_dn',
            label=_('DHCP Service Options DN'),
        ),
        Str('dhcpzonedn',
            cli_name='service_zone_dn',
            label=_('DHCP Service Zone DN'),
        ),
        Str('dhcpkeydn',
            cli_name='service_key_dn',
            label=_('DHCP Service Key DN'),
        ),
        Str('dhcpfailoverpeerdn',
            cli_name='service_fail_over_peer_dn',
            label=_('DHCP Service Fail Over Peer DN'),
        ),
        Str('dhcpstatements',
            cli_name='service_statements',
            label=_('DHcp Service Statements'),
        ),
        Str('dhcpcomments',
            cli_name='service_comments',
            label=_('DHCP Service Comments'),
        ),
        Str('dhcpoption',
            cli_name='service_option',
            label=_('DHCP Service Option'),
        ),

    )

api.register(dhcpservice)

class dhcpfailoverpeer(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Fail Over Peer')
    object_name_plural = _('DHCP Fail Over Peers')
    object_class = ['dhcpServer', 'top']
    default_attributes = ['cn', 
                        'dhcpFailOverPrimaryServer', 
                        'dhcpFailOverSecondaryServer',
                        'dhcpFailOverPrimaryPort',
                        'dhcpFailOvreSecondaryPort',]
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Fail Over Peers')
    label_singular = _('DHCP Fail Over Peer')
    takes_params = (
        Str('cn',
            cli_name='fail_over_peer_name',
            label=_('DHCP Fail Over Peer Name'),
            primary_key=True,
        ),
        Str('dhcpfailoverprimaryserver',
            cli_name='fail_over_primary_server',
            label=_('DHCP Fail Over Primary Server'),
        ),
        Str('dhcpfailoversecondaryserver',
            cli_name='fail_over_secondary_server',
            label=_('DHCP Fail Over Secondary Server'),
        ),
        Str('dhcpfailoverprimaryport',
            cli_name='fail_over_primary_port',
            label=_('DHCP Fail Over Primary Port'),
        ),
        Str('dhcpfailoversecondaryport',
            cli_name='fail_over_secondary_port',
            label=_('DHCP Fail Over Secondary Port'),
        ),
        Str('dhcpfailoverresponsedelay',
            cli_name='fail_over_response_delay',
            label=_('DHCP Fail Over Response Delay'),
        ),
        Str('dhcpfailoverunackedupdates',
            cli_name='fail_over_unacked_updates',
            label=_('DHCP Fail Over Unacked Updates'),
        ),
        Str('dhcpmaxclientleadtime',
            cli_name='fail_over_max_client_lead_time',
            label=_('DHCP Fail Over Max Client Lead Time'),
        ),
        Str('dhcpfailoversplit',
            cli_name='fail_over_split',
            label=_('DHCP Fail Over Split'),
        ),
        Str('dhcphashbucketassignment',
            cli_name='fail_over_hash_bucket_assignment',
            label=_('DHCP Fail Over Hash Bucket Assignment'),
        ),
        Str('dhcpfailoverloadbalancetime',
            cli_name='fail_over_load_balance_time',
            label=_('DHCP Fail Over Load Balance Time'),
        ),
        Str('dhcpcomments',
            cli_name='fail_over_comments',
            label=_('DHCP Fail Over Comments'),
        ),

    )

api.register(dhcpfailoverpeer)

class dhcpsharednetwork(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Shared Network')
    object_name_plural = _('DHCP Shared Networks')
    object_class = ['dhcpServer', 'top']
    default_attributes = ['cn']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Shared Networks')
    label_singular = _('DHCP Shared Network')
    takes_params = (
        Str('cn',
            cli_name='shared_network_name',
            label=_('DHCP Shared Network Name'),
            primary_key=True,
        ),
        Str('dhcpsubnetdn',
            cli_name='shared_network_subnet_dn',
            label=_('DHCP Shared Network Subnet DN'),
        ),
        Str('dhcppooldn',
            cli_name='shared_network_pool_dn',
            label=_('DHCP Shared Network Pool DN'),
        ),
        Str('dhcpoptionsdn',
            cli_name='shared_network_options_dn',
            label=_('DHCP Shared Network Options DN'),
        ),
        Str('dhcpzonedn',
            cli_name='shared_network_zone_dn',
            label=_('DHCP Shared Network Zone DN'),
        ),
        Str('dhcpstatements',
            cli_name='shared_network_statements',
            label=_('DHCP Shared Network Statements'),
        ),
        Str('dhcpcomments',
            cli_name='shared_network_comments',
            label=_('DHCP Shared Network Comments'),
        ),
        Str('dhcpoption',
            cli_name='shared_network_option',
            label=_('DHCP Shared Network Option'),
        ),

    )

api.register(dhcpsharednetwork)

class dhcpsubnet(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Subnet')
    object_name_plural = _('DHCP Subnets')
    object_class = ['dhcpSubnet', 'dhcpOptions', 'top']
    default_attributes = ['cn', 'dhcpNetMask']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Subnets')
    label_singular = _('DHCP Subnet')
    takes_params = (
        Str('cn',
            cli_name='subnet_name',
            label=_('DHCP Subnet Name'),
            primary_key=True,
        ),
        Str('dhcpnetmask',
            cli_name='subnet_netmask',
            label=_('DHCP Subnet Netmask'),
        ),
        Str('dhcprange',
            cli_name='subnet_range',
            label=_('DHCP Subnet Range'),
        ),
        Str('dhcppooldn',
            cli_name='subnet_pool_dn',
            label=_('DHCP Subnet Pool DN'),
        ),
        Str('dhcpgroupdn',
            cli_name='subnet_group_dn',
            label=_('DHCP Subnet Group DN'),
        ),
        Str('dhcphostdn',
            cli_name='subnet_host_dn',
            label=_('DHCP Subnet Host DN'),
        ),
        Str('dhcpclassesdn',
            cli_name='subnet_classes_dn',
            label=_('DHCP Subnet Classes DN'),
        ),
        Str('dhcpleasesdn',
            cli_name='subnet_leases_dn',
            label=_('DHCP Subnet Leases DN'),
        ),
        Str('dhcpoptionsdn',
            cli_name='subnet_options_dn',
            label=_('DHCP Subnet Options DN'),
        ),
        Str('dhcpzonedn',
            cli_name='subnet_zone_dn',
            label=_('DHCP Subnet Zone DN'),
        ),
        Str('dhcpkeydn',
            cli_name='subnet_key_dn',
            label=_('DHCP Subnet Key DN'),
        ),
        Str('dhcpfailoverpeerdn',
            cli_name='subnet_fail_over_peer_dn',
            label=_('DHCP Subnet Fail Over Peer DN'),
        ),
        Str('dhcpstatements',
            cli_name='subnet_statements',
            label=_('DHCP Subnet Statements'),
        ),
        Str('dhcpcomments',
            cli_name='subnet_comments',
            label=_('DHCP Subnet Comments'),
        ),
        Str('dhcpoption',
            cli_name='subnet_option',
            label=_('DHCP Subnet Option'),
        ),

    )

api.register(dhcpsubnet)

class dhcppool(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Pool')
    object_name_plural = _('DHCP Pools')
    object_class = ['dhcpPool', 'top']
    default_attributes = ['cn', 'dhcpRange']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Pools')
    label_singular = _('DHCP Pool')
    takes_params = (
        Str('cn',
            cli_name='pool_name',
            label=_('DHCP Pool Name'),
            primary_key=True,
        ),
        Str('dhcprange',
            cli_name='pool_range',
            label=_('DHCP Pool Range'),
        ),
        Str('dhcpclassesdn',
            cli_name='pool_classes_dn',
            label=_('DHCP Pool Classes DN'),
        ),
        Str('dhcppermitlist',
            cli_name='pool_permit_list',
            label=_('DHCP Pool Permit List'),
        ),
        Str('dhcpleasesdn',
            cli_name='pool_leases_dn',
            label=_('DHCP Pool Leases DN'),
        ),
        Str('dhcpoptionsdn',
            cli_name='pool_options_dn',
            label=_('DHCP Pool Options DN'),
        ),
        Str('dhcpzonedn',
            cli_name='pool_zone_dn',
            label=_('DHCP Pool Zone DN'),
        ),
        Str('dhcpkeydn',
            cli_name='pool_key_dn',
            label=_('DHCP Pool Key DN'),
        ),
        Str('dhcpstatements',
            cli_name='pool_statements',
            label=_('DHCP Pool Statements'),
        ),
        Str('dhcpcomments',
            cli_name='pool_comments',
            label=_('DHCP Pool Comments'),
        ),
        Str('dhcpoption',
            cli_name='pool_option',
            label=_('DHCP Pool Option'),
        ),

    )

api.register(dhcppool)

class dhcpgroup(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Group')
    object_name_plural = _('DHCP Groups')
    object_class = ['dhcpGroup', 'top']
    default_attributes = ['cn']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Groups')
    label_singular = _('DHCP Group')
    takes_params = (
        Str('cn',
            cli_name='group_name',
            label=_('DHCP Group Name'),
            primary_key=True,
        ),
        Str('dhcphostdn',
            cli_name='group_host_dn',
            label=_('DHCP Group Host DN'),
        ),
        Str('dhcpoptionsdn',
            cli_name='group_options_dn',
            label=_('DHCP Group Options DN'),
        ),
        Str('dhcpstatements',
            cli_name='group_statements',
            label=_('DHCP Group Statements'),
        ),
        Str('dhcpcomments',
            cli_name='group_comments',
            label=_('DHCP Group Comments'),
        ),
        Str('dhcpoption',
            cli_name='group_option',
            label=_('DHCP Group Option'),
        ),

    )

api.register(dhcpgroup)
    
class dhcphost(LDAPObject):
    container_dn = api.env.container_dhcp
    object_name = _('DHCP Host')
    object_name_plural = _('DHCP Hosts')
    object_class = ['dhcpHost', 'top']
    default_attributes = ['cn']
    #uuid_attribute
    #rdn_attribute
    #attributer_members
    label = _('DHCP Hosts')
    label_singular = _('DHCP Host')
    takes_params = (
        Str('cn',
            cli_name='host_name',
            label=_('DHCP Host Name'),
            primary_key=True,
        ),
        Str('dhcpleasedn',
            cli_name='host_lease_dn',
            label=_('DHCP Host Lease DN'),
        ),
        Str('dhcphwaddress',
            cli_name='host_hwaddress',
            label=_('DHCP Host HWAddress'),
        ),
        Str('dhcpoptionsdn',
            cli_name='host_options_dn',
            label=_('DHCP Host Options DN'),
        ),
        Str('dhcpstatements',
            cli_name='host_statements',
            label=_('DHCP Host Statements'),
        ),
        Str('dhcpcomments',
            cli_name='host_comments',
            label=_('DHCP Host Comments'),
        ),
        Str('dhcpoption',
            cli_name='host_option',
            label=_('DHCP Host Option'),
        ),

    )

api.register(dhcphost)
