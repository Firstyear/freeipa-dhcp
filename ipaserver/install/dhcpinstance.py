# Authors: William Brown <william@firstyear.id.au>
#
# Copyright (C) 2007  Red Hat
# see file 'COPYING' for use and warranty information
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import installutils
import ldap
import service
import sys

import ipalib
from ipalib import api, util, errors
from ipapython import sysrestore

def _ldap_object_exists(conn, dn):
    try:
        conn.search_ext_s(dn, ldap.SCOPE_BASE)
    except ldap.NO_SUCH_OBJECT:
        return False
    return True

def check_prepared(unattended):
    pass

def check_inst(unattended):
    pass

class DhcpInstance(service.Service):
    ## This setups the DHCP config, creates the service account and registers the server 
    def __init__(self, fstore=None, dm_password=None):
        service.Service.__init__(self, "dhcpd", dm_password=dm_password)
        if fstore:
            self.fstore = fstore
        else:
            self.fstore = sysrestore.FileStore('/var/lib/ipa/sysrestore')
   
    def setup(self, fqdn, ipaddress, v4=False, v6=False, location="Default"):
        self.fqdn = fqdn
        self.ipaddress = ipaddress
        self.location = location
        self.v4 = v4
        self.v6 = v6

    def create_instance(self):
        try:
            self.stop()
        except:
            pass

        # Now, we check if -v4 was specified. If yes, we install a dhcpv4 server instance on this server.
        # If -v6 was specificed, we install a dhcp6 instance into this location. 
        #
        # If this machine does not yet have a generic account for accessing the DHCP as a server, we need to add it, and add the permission to this account for DHCP server read, and leases write.
        # 
        if not self._check_dhcp_container():
            sys.exit("\nDHCP has not been prepared in this domain. Have you run ipa-dhcp-prepare?")
        if self.v4:
            self.step("Creating dhcp instance", self.__create_v4_instance)
        if self.v6:
            self.step("Creating dhcp6 instance", self.__create_v6_instance)
        if self.v4:
            self.step("Starting dhcp", self.__start_dhcp)
            self.step("Registering dhcp to start at boot", self.__enable_dhcp)
        if self.v6:
            self.step("Starting dhcp6", self.__start_dhcp6)
            self.step("Resgistering dhcp6 to start at boot", self.__enable_dhcp6)
        # Post install we need to warn the user about opening certain firewall ports.
        # If v4, we need port 68 for outbound / inbound for DHCP
        # If v6, we need port 546 (I think) For ipv6 on a specific fe80::/16 subnet.
        self.start_creation("Configuring dhcp:")

    def uninstall(self):
        pass

    def __create_location(self):
        #This should be calling the ipa.api.command.dhcp_addlocation
        #This addition of a location adds the v4 and v6 branches 
        pass

    def __create_v4_instance(self):
        #This should add the server to the ldap, in the cn=v4,cn=dhcp, with the correct DHCPServer objects.
        #This should then create the relevant files on disk for this server.
        pass

    def __create_v6_instance(self):
        #This should add the server to the ldap in the cn=v6,cn=dhcp container, with the correct DHCP server objects.
        #This should create the relevant files on disk for this server.
        pass

    def __start_dhcp(self):
        pass

    def __start_dhcp6(self):
        pass

    def __enable_dhcp(self):
        pass

    def __enable_dhcp6(self):
        pass

    def _check_dhcp_container(self):
        #Check that our dhcp container exists cn=dhcp,dc=foo,dc=bar
        pass

    def _check_dhcp_location(self):
        #Check the location exists. 
        #Check that there are not more than 2 servers already in this location. This will need to be done by checking their configuration dns
        pass

    def _check_dhcp_preinstalled(self):
        pass



class DhcpIpaInstance(service.Service):
    ## This adds our base objects and ACI's to the server. After this, we should be able to make DHCP locations etc.
    def __init__(self, fstore=None, dm_password=None):
        service.Service.__init__(self, "dhcpd", dm_password=dm_password)
        if fstore:
            self.fstore = fstore
        else:
            self.fstore = sysrestore.FileStore('/var/lib/ipa/sysrestore')

    def setup(self):
        pass

    def create_instance(self):
        try:
            self.stop()
        except:
            pass
        
        #self.ldap_connect()
        #Check if we already have the DHCP container setup. If not, install it.
        # This is also a check of the permissions of the use we are running as.
        if not self._check_dhcp_container():
            self.step("Creating base DHCP objects", self.__setup_dhcp_ldap)
       
        ########
        # What is below this should be for the dhcp-install - above is dhcp-prepare
        ########
    def uninstall(self):
        pass

    def _check_dhcp_container(self):
        pass

    def __setup_dhcp_ldap(self):
        #sub_dict is list of WORDS in the ldif to replace.
        #self._ldap_mod("dhcpd.ldif", self.sub_dict
        pass
