#! /usr/bin/python -E
# Authors: William Brown <william@firstyear.id.au>
# Based on ipa-server-install by Karl MacMillan <kmacmillan@mentalrootkit.com>
#
# Copyright (C) 2007 - 2009  Red Hat
# see file 'COPYING' for use and warranty information
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ipapython.config import IPAOptionParser
from optparse import OptionGroup, SUPPRESS_HELP
from ipaserver.install import dhcpinstance
from ipaserver.install import installutils
from ipapython import version
from ipaserver.install.installutils import *
from ipalib import api, errors, util     


log_file_name = "/var/log/ipaserver-install.log"

#Setup options parsing.
def parse_options():
    parser = IPAOptionParser(version=version.VERSION)
    parser.add_option("-p", "--ds-password", dest="dm_password",
                      sensitive=True, help="admin password")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="print debugging information")

    options, args = parser.parse_args()
    safe_options = parser.get_safe_opts(options)


    return safe_options, options

#Pass the options to our DHCP instance setup tool.
def main():
    safe_options, options = parse_options()

    if os.getegid() != 0:
        sys.exit("Must be root to setup server")

    standard_logging_setup(log_file_name, debug=options.debug, filemode='a')
    print "\nThe log file for this installation can be found in %s" % log_file_name

    root_logger.debug('%s was invoked with options: %s' % (sys.argv[0], safe_options))
    root_logger.debug("missing options might be asked for interactively later\n")

    installutils.check_server_configuration()

    global fstore
    fstore = sysrestore.FileStore('/var/lib/ipa/sysrestore')

    print "=============================================================================="
    print "This program will setup DHCP services for this system, that will read from"
    print " the FreeIPA directory service."
    print ""
    print "This includes:"
    print "  * Configure DHCP (dhcpd)"
    print "  * Configure DHCP6 (dhcpd6)"
    print "  * Configure Router Advertisment (radvd)"
    print ""
    print "To accept the default shown in brackets, press the Enter key."
    print ""

    # Check dhcp packages are installed
    if not dhcpinstance.check_inst(options.unattended):
        sys.exit("Aborting installation.")

    # Initialize the ipalib api
    cfg = dict(
        in_server=True,
        debug=options.debug,
    )
    api.bootstrap(**cfg)
    api.finalize()

    #if dhcpinstance.named_conf_exists():
    #    sys.exit("\nDHCP is already configured in this IPA server.")

if __name__ == '__main__':
    installutils.run_script(main, log_file_name=log_file_name,
        operation_name='ipa-dhcp-install')

