#!/usr/bin/python
#
# Authors:
#   Rob Crittenden <rcritten@redhat.com>
#
# Copyright (C) 2009  Red Hat
# see file 'COPYING' for use and warranty information
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Upgrade configuration files to a newer template.
"""

import sys
try:
    from ipapython import ipautil, sysrestore, version
    from ipapython.config import IPAOptionParser
    from ipapython.ipa_log_manager import *
    from ipapython import certmonger
    from ipapython import dogtag
    from ipaserver.install import installutils
    from ipaserver.install import dsinstance
    from ipaserver.install import httpinstance
    from ipaserver.install import memcacheinstance
    from ipaserver.install import bindinstance
    from ipaserver.install import service
    from ipaserver.install import cainstance
    from ipaserver.install import certs
    from ipaserver.install import sysupgrade
    import ldap
    import krbV
    import re
    import os
    import shutil
    import fileinput
    from ipalib import api
    import ipalib.errors
except ImportError:
    print >> sys.stderr, """\
There was a problem importing one of the required Python modules. The
error was:

    %s
""" % sys.exc_value
    sys.exit(1)

def parse_options():
    parser = IPAOptionParser(version=version.VERSION)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="print debugging information")

    options, args = parser.parse_args()
    safe_options = parser.get_safe_opts(options)

    return safe_options, options

class KpasswdInstance(service.SimpleServiceInstance):
    def __init__(self):
        service.SimpleServiceInstance.__init__(self, "ipa_kpasswd")

def uninstall_ipa_kpasswd():
    """
    We can't use the full service uninstaller because that will attempt
    to stop and disable the service which by now doesn't exist. We just
    want to clean up sysrestore.state to remove all references to
    ipa_kpasswd.
    """
    ipa_kpasswd = KpasswdInstance()

    running = ipa_kpasswd.restore_state("running")
    enabled = not ipa_kpasswd.restore_state("enabled")

    if enabled is not None and not enabled:
        ipa_kpasswd.remove()

def backup_file(filename, ext):
    """Make a backup of filename using ext as the extension. Do not overwrite
       previous backups."""
    if not os.path.isabs(filename):
        raise ValueError("Absolute path required")

    backupfile = filename + ".bak"
    (reldir, file) = os.path.split(filename)

    while os.path.exists(backupfile):
        backupfile = backupfile + "." + str(ext)

    try:
        shutil.copy2(filename, backupfile)
    except IOError, e:
        if e.errno == 2: # No such file or directory
            pass
        else:
            raise e

def update_conf(sub_dict, filename, template_filename):
    template = ipautil.template_file(template_filename, sub_dict)
    fd = open(filename, "w")
    fd.write(template)
    fd.close()

def find_hostname():
    """Find the hostname currently configured in ipa-rewrite.conf"""
    filename="/etc/httpd/conf.d/ipa-rewrite.conf"

    if not ipautil.file_exists(filename):
        return None

    pattern = "^[\s#]*.*https:\/\/([A-Za-z0-9\.\-]*)\/.*"
    p = re.compile(pattern)
    for line in fileinput.input(filename):
        if p.search(line):
            fileinput.close()
            return p.search(line).group(1)
    fileinput.close()

    raise RuntimeError("Unable to determine the fully qualified hostname from %s" % filename)

def find_autoredirect(fqdn):
    """
    When upgrading ipa-rewrite.conf we need to see if the automatic redirect
    was disabled during install time (or afterward). So sift through the
    configuration file and see if we can determine the status.

    Returns True if autoredirect is enabled, False otherwise
    """
    filename = '/etc/httpd/conf.d/ipa-rewrite.conf'
    if os.path.exists(filename):
        pattern = "^RewriteRule \^/\$ https://%s/ipa/ui \[L,NC,R=301\]" % fqdn
        p = re.compile(pattern)
        for line in fileinput.input(filename):
            if p.search(line):
                fileinput.close()
                return True
        fileinput.close()
        return False
    return True

def find_version(filename):
    """Find the version of a configuration file"""
    if os.path.exists(filename):
        pattern = "^[\s#]*VERSION\s+([0-9]+)\s+.*"
        p = re.compile(pattern)
        for line in fileinput.input(filename):
            if p.search(line):
                fileinput.close()
                return p.search(line).group(1)
        fileinput.close()

        # no VERSION found
        return 0
    else:
        return -1

def upgrade(sub_dict, filename, template, add=False):
    """
    Get the version from the current and template files and update the
    installed configuration file if there is a new template.

    If add is True then create a new configuration file.
    """
    old = int(find_version(filename))
    new = int(find_version(template))

    if old < 0 and not add:
        root_logger.error("%s not found." % filename)
        sys.exit(1)

    if new < 0:
        root_logger.error("%s not found." % template)

    if old < new or (add and old == 0):
        backup_file(filename, new)
        update_conf(sub_dict, filename, template)
        root_logger.info("Upgraded %s to version %d", filename, new)

def check_certs():
    """Check ca.crt is in the right place, and try to fix if not"""
    root_logger.info('[Verifying that root certificate is published]')
    if not os.path.exists("/usr/share/ipa/html/ca.crt"):
        ca_file = "/etc/httpd/alias/cacert.asc"
        if os.path.exists(ca_file):
            old_umask = os.umask(022)   # make sure its readable by httpd
            try:
                shutil.copyfile(ca_file, "/usr/share/ipa/html/ca.crt")
            finally:
                os.umask(old_umask)
        else:
            root_logger.error("Missing Certification Authority file.")
            root_logger.error("You should place a copy of the CA certificate in /usr/share/ipa/html/ca.crt")
    else:
        root_logger.debug('Certificate file exists')

def upgrade_pki(fstore):
    """
    Update/add the dogtag proxy configuration. The IPA side of this is
    handled in ipa-pki-proxy.conf.

    This requires enabling SSL renegotiation.
    """
    root_logger.info('[Verifying that CA proxy configuration is correct]')
    if not os.path.exists('/etc/pki-ca/CS.cfg'):
        root_logger.debug('No CA detected in /etc/pki-ca')
        return

    http = httpinstance.HTTPInstance(fstore)
    http.enable_mod_nss_renegotiate()
    if not installutils.get_directive('/etc/pki-ca/CS.cfg',
                                      'proxy.securePort', '=') and \
            os.path.exists('/usr/bin/pki-setup-proxy'):
        ipautil.run(['/usr/bin/pki-setup-proxy', '-pki_instance_root=/var/lib'
                     ,'-pki_instance_name=pki-ca','-subsystem_type=ca'])
        root_logger.debug('Proxy configuration updated')
    else:
        root_logger.debug('Proxy configuration up-to-date')

def update_dbmodules(realm, filename="/etc/krb5.conf"):
    newfile = []
    found_dbrealm = False
    found_realm = False
    prefix = ''

    root_logger.info('[Verifying that KDC configuration is using ipa-kdb backend]')
    st = os.stat(filename)
    fd = open(filename)

    lines = fd.readlines()
    fd.close()

    if '    db_library = ipadb.so\n' in lines:
        root_logger.debug('dbmodules already updated in %s', filename)
        return

    for line in lines:
        if line.startswith('[dbmodules]'):
            found_dbrealm = True
        if found_dbrealm and line.find(realm) > -1:
            found_realm = True
            prefix = '#'
        if found_dbrealm and line.find('}') > -1 and found_realm:
            found_realm = False
            newfile.append('#%s' % line)
            prefix = ''
            continue

        newfile.append('%s%s' % (prefix, line))

    # Append updated dbmodules information
    newfile.append('  %s = {\n' % realm)
    newfile.append('    db_library = ipadb.so\n')
    newfile.append('  }\n')

    # Write out new file
    fd = open(filename, 'w')
    fd.write("".join(newfile))
    fd.close()
    root_logger.debug('%s updated', filename)

def cleanup_kdc(fstore):
    """
    Clean up old KDC files if they exist. We need to remove the actual
    file and any references in the uninstall configuration.
    """
    root_logger.info('[Checking for deprecated KDC configuration files]')
    for file in ['kpasswd.keytab', 'ldappwd']:
        filename = '/var/kerberos/krb5kdc/%s' % file
        installutils.remove_file(filename)
        if fstore.has_file(filename):
            fstore.untrack_file(filename)
            root_logger.debug('Uninstalling %s', filename)

def upgrade_ipa_profile(realm):
    """
    Update the IPA Profile provided by dogtag
    """
    root_logger.info('[Verifying that CA service certificate profile is updated]')
    ca = cainstance.CAInstance(realm, certs.NSS_DIR)
    if ca.is_configured():
        if ca.enable_subject_key_identifier():
            root_logger.debug('Subject Key Identifier updated, restarting CA')
            ca.restart()
        else:
            root_logger.debug('Subject Key Identifier already set.')
    else:
        root_logger.debug('CA is not configured')

def upgrade_httpd_selinux(fstore):
    """
    Update SElinux configuration for httpd instance in the same way as the
    new server installation does.
    """
    root_logger.info('[Verifying the Apache SELinux configuration]')
    http = httpinstance.HTTPInstance(fstore)
    http.configure_selinux_for_httpd()

def named_enable_psearch():
    """
    From IPA 3.0, persistent search is a preferred mechanism for new DNS zone
    detection and is also needed for other features (DNSSEC, SOA serial
    updates). Enable psearch and make sure connections attribute is right.
    This step is done just once for a case when user switched the persistent
    search back to disabled.

    When some change in named.conf is done, this functions returns True
    """
    changed = False

    root_logger.info('[Enabling persistent search in DNS]')

    if not bindinstance.named_conf_exists():
        # DNS service may not be configured
        root_logger.debug('DNS not configured')
        return

    try:
        psearch = bindinstance.named_conf_get_directive('psearch')
    except IOError, e:
        root_logger.debug('Cannot retrieve psearch option from %s: %s',
                bindinstance.NAMED_CONF, e)
        return
    else:
        psearch = None if psearch is None else psearch.lower()
    if not sysupgrade.get_upgrade_state('named.conf', 'psearch_enabled'):
        if psearch != "yes":
            try:
                bindinstance.named_conf_set_directive('zone_refresh', 0)
                bindinstance.named_conf_set_directive('psearch', 'yes')
            except IOError, e:
                root_logger.error('Cannot enable psearch in %s: %s',
                        bindinstance.NAMED_CONF, e)
            else:
                changed = True
                psearch = "yes"
        sysupgrade.set_upgrade_state('named.conf', 'psearch_enabled', True)
        root_logger.debug('Persistent search enabled')

    # make sure number of connections is right
    minimum_connections = 2
    if psearch == 'yes':
        # serial_autoincrement increased the minimal number of connections to 4
        minimum_connections = 4
    try:
        connections = bindinstance.named_conf_get_directive('connections')
    except IOError, e:
        root_logger.debug('Cannot retrieve connections option from %s: %s',
                bindinstance.NAMED_CONF, e)
        return
    try:
        if connections is not None:
            connections = int(connections)
    except ValueError:
        # this should not happend, but there is some bad value in
        # "connections" option, bail out
        pass
    else:
        if connections is None or connections < minimum_connections:
            try:
                bindinstance.named_conf_set_directive('connections',
                                                        minimum_connections)
                root_logger.debug('Connections set to %d', minimum_connections)
            except IOError, e:
                root_logger.error('Cannot update connections in %s: %s',
                        bindinstance.NAMED_CONF, e)
            else:
                changed = True

    if not changed:
        root_logger.debug('No changes made')
    return changed

def named_enable_serial_autoincrement():
    """
    Serial autoincrement is a requirement for zone transfers or DNSSEC. It
    should be enabled both for new installs and upgraded servers.

    When some change in named.conf is done, this functions returns True
    """
    changed = False

    root_logger.info('[Enabling serial autoincrement in DNS]')

    if not bindinstance.named_conf_exists():
        # DNS service may not be configured
        root_logger.debug('DNS not configured')
        return changed

    try:
        psearch = bindinstance.named_conf_get_directive('psearch')
        serial_autoincrement = bindinstance.named_conf_get_directive(
                                    'serial_autoincrement')
    except IOError, e:
        root_logger.debug('Cannot retrieve psearch option from %s: %s',
                bindinstance.NAMED_CONF, e)
        return changed
    else:
        psearch = None if psearch is None else psearch.lower()
        serial_autoincrement = None if serial_autoincrement is None \
                else serial_autoincrement.lower()

    # enable SOA serial autoincrement
    if not sysupgrade.get_upgrade_state('named.conf', 'autoincrement_enabled'):
        if psearch != "yes":    # psearch is required
            root_logger.error('Persistent search is disabled, '
                    'serial autoincrement cannot be enabled')
        else:
            if serial_autoincrement != 'yes':
                try:
                    bindinstance.named_conf_set_directive('serial_autoincrement', 'yes')
                except IOError, e:
                    root_logger.error('Cannot enable serial_autoincrement in %s: %s',
                            bindinstance.NAMED_CONF, e)
                    return changed
                else:
                    root_logger.debug('Serial autoincrement enabled')
                    changed = True
            else:
                root_logger.debug('Serial autoincrement is alredy enabled')
        sysupgrade.set_upgrade_state('named.conf', 'autoincrement_enabled', True)
    else:
        root_logger.debug('Skip serial autoincrement check')

    return changed

def enable_certificate_renewal(realm):
    """
    If the CA subsystem certificates are not being tracked for renewal then
    tell certmonger to start tracking them.
    """
    ca = cainstance.CAInstance(realm, certs.NSS_DIR)
    if not ca.is_configured():
        root_logger.debug('dogtag not configured')
        return

    # Using the nickname find the certmonger request_id
    criteria = (('cert_storage_location', '/etc/httpd/alias', certmonger.NPATH),('cert_nickname', 'ipaCert', None))
    request_id = certmonger.get_request_id(criteria)
    if request_id is not None:
        root_logger.debug('Certificate renewal already configured')
        return

    if not sysupgrade.get_upgrade_state('dogtag', 'renewal_configured'):
        if ca.is_master():
            ca.configure_renewal()
        else:
            ca.configure_certmonger_renewal()
            ca.configure_clone_renewal()
            ca.configure_agent_renewal()
        ca.track_servercert()
        sysupgrade.set_upgrade_state('dogtag', 'renewal_configured', True)
        ca.restart(dogtag.configured_constants().PKI_INSTANCE_NAME)
        root_logger.debug('CA subsystem certificate renewal enabled')

def main():
    """
    Get some basics about the system. If getting those basics fail then
    this is likely because the machine isn't currently an IPA server so
    exit gracefully.
    """

    if not os.geteuid()==0:
        sys.exit("\nYou must be root to run this script.\n")

    if not installutils.is_ipa_configured():
        sys.exit(0)

    safe_options, options = parse_options()

    standard_logging_setup('/var/log/ipaupgrade.log', verbose=True,
        debug=options.debug, console_format='%(message)s',
        filemode='a')

    fstore = sysrestore.FileStore('/var/lib/ipa/sysrestore')

    api.bootstrap(context='restart')
    api.finalize()

    fqdn = find_hostname()
    if fqdn is None:
        # ipa-rewrite.conf doesn't exist, nothing to do
        sys.exit(0)

    # Ok, we are an IPA server, do the additional tests

    check_certs()

    auto_redirect = find_autoredirect(fqdn)
    configured_constants = dogtag.configured_constants()
    sub_dict = dict(
        REALM=api.env.realm,
        FQDN=fqdn,
        AUTOREDIR='' if auto_redirect else '#',
        CRL_PUBLISH_PATH=configured_constants.CRL_PUBLISH_PATH,
        DOGTAG_PORT=configured_constants.AJP_PORT,
    )

    upgrade(sub_dict, "/etc/httpd/conf.d/ipa.conf", ipautil.SHARE_DIR + "ipa.conf")
    upgrade(sub_dict, "/etc/httpd/conf.d/ipa-rewrite.conf", ipautil.SHARE_DIR + "ipa-rewrite.conf")
    upgrade(sub_dict, "/etc/httpd/conf.d/ipa-pki-proxy.conf", ipautil.SHARE_DIR + "ipa-pki-proxy.conf", add=True)
    upgrade_pki(fstore)
    update_dbmodules(api.env.realm)
    uninstall_ipa_kpasswd()

    http = httpinstance.HTTPInstance(fstore)
    http.remove_httpd_ccache()
    http.configure_selinux_for_httpd()

    memcache = memcacheinstance.MemcacheInstance()
    memcache.ldapi = True
    memcache.realm = api.env.realm
    try:
        if not memcache.is_configured():
            # 389-ds needs to be running to create the memcache instance
            # because we record the new service in cn=masters.
            ds = dsinstance.DsInstance()
            ds.start()
            memcache.create_instance('MEMCACHE', fqdn, None, ipautil.realm_to_suffix(api.env.realm))
    except (ldap.ALREADY_EXISTS, ipalib.errors.DuplicateEntry):
        pass

    cleanup_kdc(fstore)
    upgrade_ipa_profile(api.env.realm)
    changed_psearch = named_enable_psearch()
    changed_autoincrement = named_enable_serial_autoincrement()
    if changed_psearch or changed_autoincrement:
        # configuration has changed, restart the name server
        root_logger.info('Changes to named.conf have been made, restart named')
        bindinstance.BindInstance(fstore).restart()
    enable_certificate_renewal(api.env.realm)

if __name__ == '__main__':
    installutils.run_script(main, operation_name='ipa-upgradeconfig')
